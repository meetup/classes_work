import argparse
from weather import WeatherApi

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'city',
        help="Enter the city name to find out the weather"
    )
    parser.add_argument(
        'country',
        help="Enter the country where the city is located"
    )

    args = parser.parse_args()
    weather_details = WeatherApi(args.city, args.country)
    data = weather_details.get_city_weather()
    print("Desc - ", data["weather"][0]["description"])


if __name__ == "__main__":
    main()


