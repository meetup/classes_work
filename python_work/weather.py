import requests


class WeatherApi(object):

    BASE_URL = "http://api.openweathermap.org/data/2.5/"
    API_KEY = "804426a65b8cce4d37a4ea07531bf160"
    URL_SWITCH = ""
    city_data = {}


    def __init__(self, city, country):
        self.city = city
        self.country = country
        self.url = self.BASE_URL + self.URL_SWITCH


    def get_city_weather(self):

        self.URL_SWITCH = "weather"
        location = "{0}, {1}".format(self.city, self.country)
        payload = {
            "q":location,
            "APPID":self.API_KEY,
            "units":"metric"
        }

        request = requests.get(self.url + self.URL_SWITCH, params=payload)
        self.city_data = request.json()
        #print(self.city_data)
        return self.city_data
